class Formations {
    constructor(id, title, description, duration, type) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.type = type;
    }
}
const formations = [
    new Formations(0, "MongoDB1", "formation01", "20 heures", "MongoDB"),
    new Formations(1, "MongoDB2", "formation02", "10 heures", "MongoDB"),
    new Formations(2, "MongoDB3", "formation03", "30 heures", "MongoDB"),
    new Formations(3, "MongoDB4", "formation04", "15 heures", "MongoDB"),
    new Formations(4, "MongoDB5", "formation05", "25 heures", "MongoDB"),
    new Formations(5, "Nodejs1", "formation01", "2 heures", "Nodejs"),
    new Formations(6, "Nodejs2", "formation02", "1 heures", "Nodejs"),
    new Formations(7, "Nodejs3", "formation03", "3 heures", "Nodejs"),
    new Formations(8, "Nodejs4", "formation04", "1 heures", "Nodejs"),
    new Formations(9, "Nodejs5", "formation05", "2 heures", "Nodejs"),
];
exports.getFormationById = function(id) {
    return formations[id];
}

exports.addFormation = function(data) {
    let id = data.length;
    data.push(new Formations(id, data.title, data.description, data.duration, data.type));
    return true;
}


exports.allFormations = formations;