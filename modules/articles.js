class Articles {
    constructor(id, title, description, text) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.text = text;
    }
}
const articles = [
    new Articles(0, "Article 1", "Article super intéressant", "Lorem ipsum"),
    new Articles(1, "Article 2", "Article très court", "Lorem ipsum"),
    new Articles(2, "Article 3", "Article moyen", "Lorem ipsum"),
    new Articles(3, "Article 4", "Article encore en Lorem ipsum", "Lorem ipsum"),
    new Articles(4, "Article 5", "Article par Mme.Pepperpotte", "Lorem ipsum"),
    new Articles(5, "Article 6", "Article par M.Bean", "Lorem ipsum"),
    new Articles(6, "Article 7", "Article par Mme.Otter", "Lorem ipsum"),
    new Articles(7, "Article 8", "Article par M.Magoo", "Lorem ipsum"),
    new Articles(8, "Article 9", "Article M.Bontemps", "Lorem ipsum"),
    new Articles(9, "Article 10", "Dernier article très pertinent", "Lorem ipsum"),
];

exports.getArticleById = function(id) {
    return articles[id];
}
exports.addArticle = function(data) {
    let id = data.length;
    data.push(new Articles(id, data.title, data.description, data.text));
    return true;
}
exports.allArticles = articles;