var articles = require('../modules/articles');

exports.index = function(req, res, next) {
    res.render('index', {
        title: 'Articles',
        articles: article.allArticles
    });
}

/*callback*/
exports.add = function(req, res, next) {
    res.render("addArticle");
}
exports.addArticle = function(req, res, next) {
    articles.addArticle(req.body);
    res.redirect('/');
}
exports.getArticleById = function(req, res, next) {
    res.render('article', { article: articles.getArticleById(req.params.id) });
}