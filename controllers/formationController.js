var formations = require('../modules/formations');

exports.index = function(req, res, next) {
    res.render('index', {
        title: 'Formations',
        formations: formation.allFormations
    });
}

/*callback*/
exports.add = function(req, res, next) {
    res.render("addFormation");
}
exports.addFormation = function(req, res, next) {
    formations.addFormation(req.body);
    res.redirect('/');
}
exports.getFormationById = function(req, res, next) {
    res.render('formation', { formation: formations.getFormationById(req.params.id) });
}