var express = require('express');
var router = express.Router();

let formations = require('../modules/formations');
var formationController = require('../controllers/formationController');

let articles = require('../modules/articles');
var articleController = require('../controllers/articleController');


/*GET Formulaire de contact */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Formulaire de contact' })
});
router.get('/formulaire/contact', function(res, res, next) {
    res.render('formulaire/contact', { title: 'Formulaire de contact' })
});
router.get('/formulaire/contact/:id', function(req, res, next) {
    res.render('contact', { contact: contacts.getContactById(req.params.id) })
});


/*GET Articles de blog */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Articles de Blog', description: 'Les 10 articles les plus pertinents' })
});
router.get('/blog/articles', function(req, res, next) {

    res.render('blog/articles', { title: 'Articles de Blog', description: 'Les 10 articles les plus pertinents', articles: articles.allArticles })
});

router.get('/blog/articles/:id', function(req, res, next) {
    res.render('article', { article: articles.getArticleById(req.params.id) })
});


/*GET formation*/
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Nos formations', description: 'Nos formations MongoDB et Nodejs' })
});
router.get('/formation/formations', function(req, res, next) {

    res.render('formation/formations', { title: 'Nos formations', description: 'Nos formations MongoDB et Nodejs', formations: formations.allFormations })
});

router.get('/formation/formations/:id', function(req, res, next) {
    res.render('formation', { formation: formations.getFormationById(req.params.id) })
});

/*formation*/
router.get('/', formationController.index);

router.get('/formation/formations/add', formationController.add);

router.get('/formation/formations/add', formationController.addFormation);

router.get('/formation/formations/:method/:id', formationController.getFormationById);



/*article(blog)*/
router.get('/', articleController.index);

router.get('/blog/article/add', articleController.add);

router.get('/blog/article/add', articleController.addArticle);

router.get('/blog/article/:method/:id', articleController.getArticleById);

/*formulaire*/
router.post('/formulaire/contact/add', function(req, res, next) {
    let formulaire = contact.addContact(req.body)
    if (formulaire) {
        res.render('contact', { title: 'formulaire de contact' })
    }
});


module.exports = router;